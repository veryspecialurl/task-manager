from django.urls import path
from .views import *

app_name = 'tm'
urlpatterns = [
    path('<int:year>/', MainView.as_view(), name='year_page'),
    path('day/', DayView.as_view(), name='day_page'),
    path('task/', TaskView.as_view(), name='task_page'),
    path('task/<pk>', TaskView.as_view(), name='task_page'),
    path('settings/', SettingsView.as_view(), name='settings_page'),
    path('reminder/<pk>', ReminderView.as_view(), name='reminder_page'),
    path('vkbot/', BotUtils.as_view(), name='bot_utils'),
    path('vkbot/create_reminder', DaylyReminder.as_view(), name='dayly_reminder_view')
]