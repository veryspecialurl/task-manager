from django.forms import ModelForm, TimeInput, TextInput, Select, Textarea, CharField, widgets
from django import forms
from .models import TimeTask


class NewTTask(ModelForm):
    data = CharField(widget=TextInput({'class': 'InputField d-none', 'type': 'hidden', 'id': 'data-date'}))

    class Meta:
        model = TimeTask
        fields = [
            'title',
            'text',
            'color',
            'time_success',
            'time_success2'
        ]
        widgets = {
            'title': TextInput(attrs={'class': 'InputField', 'placeholder': 'Новая задача'}),
            'text': Textarea(attrs={'class': 'InputField', 'placeholder': 'Описание...'}),
            'color': TextInput(attrs={'type': 'color', 'class': 'rounded-0 p-0 InputField ColorInput'}),
            'time_success': TimeInput(
                attrs={'type': 'time', 'class': 'InputField timesuccess', 'alt': 'Начальное время'}),
            'time_success2': TimeInput(
                attrs={'type': 'time', 'class': 'InputField timesuccess2', 'alt': 'Конечное время'})
        }

    def clean(self):
        data = super().clean()
        if data.get('time_success') and data.get('time_success2'):
            if data['time_success2'] < data['time_success']:
                self.add_error('time_success2', 'Время должно быть больше чем начальное')
                raise forms.ValidationError('Неверные данные', code='Invalid data')


class DateInput(forms.Form):
    date = CharField(widget=TextInput({'class': 'InputField', 'type': 'date'}))


class DateTypeInput(widgets.Widget):
    input_type = 'data'
    template_name = 'django/forms/widgets/input.html'


class MakeReminder(forms.Form):
    date = CharField(widget=DateTypeInput({'class': 'InputField'}), required=True)
    time = CharField(widget=TimeInput({'class': 'InputField'}), required=True)
