from django.test import TestCase, RequestFactory, Client
from django.utils import timezone
from authentication.models import MyUser
from .models import TimeTask
from .views import *


class TModule(TestCase):

    def setUp(self):
        self.client = Client()
        self.current_date = timezone.now()
        self.user = MyUser.objects.create_user(email='test@email.ru', password='test')
        self.factory = RequestFactory()

    def test_main_page(self):
        request = self.factory.get(f'/{self.current_date.year}')
        request.user = self.user
        response = MainView.as_view()(request, self.current_date.year)
        self.assertEqual(response.status_code, 200)

    def test_day_info_full_fields(self):
        for i in range(0, 10):
            TimeTask.objects.create(
                user=self.user,
                title=f'Test{i}',
                text=f'test text {i}',
                date_success=self.current_date.date(),
                time_success=self.current_date.time()
            )
        self.client.login(email='test@email.ru', password='test')
        response = self.client.get(f'/day/', {'year': self.current_date.year,
                                              'month': self.current_date.month,
                                              'day': self.current_date.day})
        self.assertEqual(response.status_code, 200)
        self.assertTrue('items' in response.json())
        self.assertTrue(len(response.json()['items']) == 10)

    def test_day_info_no_fields(self):
        self.client.login(email='test@email.ru', password='test')
        response = self.client.get(f'/day/', {'year': self.current_date.year,
                                              'month': self.current_date.month})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json()['message'], 'Неправильный запрос')
        self.assertEqual(response.json()['description'], 'Не переданы поля: day')
