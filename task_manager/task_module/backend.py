import datetime
import logging
from calendar import monthrange

from .models import TimeTask

file_logger = logging.getLogger('tm.file')


class TTControl:

    # Названия месяцев в родительном падеже
    MONTH_NAMES_R = {
        '0': 'Null',
        '1': 'Января',
        '2': 'Февраля',
        '3': 'Марта',
        '4': 'Апреля',
        '5': 'Мая',
        '6': 'Июня',
        '7': 'Июля',
        '8': 'Августа',
        '9': 'Сентября',
        '10': 'Октября',
        '11': 'Ноября',
        '12': 'Декабря'
    }

    MONTH_NAMES = {
        '1': 'Январь',
        '2': 'Февраль',
        '3': 'Март',
        '4': 'Апрель',
        '5': 'Май',
        '6': 'Июнь',
        '7': 'Июль',
        '8': 'Август',
        '9': 'Сентябрь',
        '10': 'Октябрь',
        '11': 'Ноябрь',
        '12': 'Декабрь'
    }

    DAY_OF_WEEK = {
        '1': 'Пн',
        '2': 'Вт',
        '3': 'Ср',
        '4': 'Чт',
        '5': 'Пт',
        '6': 'Сб',
        '7': 'Вс'
    }

    @staticmethod
    def list_split(lst, n):
        """Yield successive n-sized chunks from lst."""
        for i in range(0, len(lst), n):
            yield lst[i:i + n]

    @staticmethod
    def make_calendar(year):
        """
        MOUTH_NAME {
            DAY_NUMBER: {
                DAY_OF_WEEK,
            }
        }

        """
        output_dict = dict({
            year: dict()
        })
        for month in range(1, 13):
            month_info = monthrange(year, month)
            day_list = list()
            day_of_week = month_info[0] + 1
            if day_of_week != 1:
                for null_days in range(1, day_of_week):
                    day_list.append(
                        dict(day_of_month=0,
                             day_of_week=TTControl.DAY_OF_WEEK[str(null_days)],
                             day_of_week_number=null_days)
                    )
            for day in range(1, month_info[1] + 1):
                day_dict = dict(day_of_month=day,
                                day_of_week=TTControl.DAY_OF_WEEK[str(day_of_week)],
                                day_of_week_number=day_of_week)
                day_list.append(day_dict)
                if datetime.datetime.now().strftime('%Y-%m-%d') == datetime.date(year=int(year), month=int(month),
                                                                                 day=int(day)).strftime('%Y-%m-%d'):
                    day_list[day_list.index(day_dict)].update(current=True)
                if day_of_week == 7:
                    day_of_week = 1
                else:
                    day_of_week += 1
            output_dict[year].update(
                {
                    month: [TTControl.MONTH_NAMES[str(month)], list(TTControl.list_split(day_list, 7))]
                }
            )
        return output_dict

    @staticmethod
    def get_count_tasks_for_day(date, user):
        return TimeTask.objects.filter(date_success=date, user=user).count()

    @staticmethod
    def set_tasks_for_days(calendar, user):
        for year, value in calendar.items():
            for month_id, month in value.items():
                for week in month[1]:
                    for day in week:
                        if int(day['day_of_month']) != 0:
                            data = datetime.date(year=int(year), month=int(month_id), day=int(day['day_of_month']))
                            task_count = TTControl.get_count_tasks_for_day(data, user)
                            if task_count != 0:
                                day.update(task_count=task_count)
        return calendar

    @staticmethod
    def get_task_list(user, date):
        tasks = TimeTask.objects.filter(user=user, date_success=date).order_by('time_success')
        return [dict(pk=task.id, title=task.title, color=task.color, text=task.text, hour=task.time_success.hour,
                     minute=task.time_success.minute, hour2=task.time_success2.hour, minute2=task.time_success2.minute)
                for task in tasks]

    @staticmethod
    def render_calendar(year, user):
        return TTControl.set_tasks_for_days(TTControl.make_calendar(year), user)

    @staticmethod
    def delete_tasks_before(user, date):
        TimeTask.objects.filter(data_success__lte=date, user=user).delete()
