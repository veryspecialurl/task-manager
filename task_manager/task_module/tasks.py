import datetime

from task_manager.celery import app
import vksdk.bots_manager as B
from authentication.models import MyUser
from .models import CTask, TimeTask
from .backend import TTControl


@app.task(bind=True)
def send_reminder(self, user_id, task_id):
    CTask.objects.get(ctask_id=self.request.id).delete()
    task = TimeTask.objects.get(id=task_id)

    message = f'&#127385; Напоминание\n_________________________________________\n{task.title}\n' \
              f'{task.text}\n' \
              f' {task.time_success.strftime("%H:%M")} - {task.time_success2.strftime("%H:%M")} |' \
              f' {task.date_success.day} {TTControl.MONTH_NAMES_R[int(task.date_success.month)]}'
    B.Message.send(user_ids=[str(user_id)], message=message)


@app.task(bind=True)
def dayly_reminder(self, user_id):
    user = MyUser.objects.get(pk=user_id)
    tasks = TimeTask.objects.filter(user=user)
    message = f'&#127385; Задачи на сегодня\n_________________________________________\n'
    for task in tasks:
        message += f"{task.name} {task.time_success}-{task.time_success2}\n"
    B.Message.send(user_ids=[user.vk['vk_credentials']['user_id']], message=message)
    day_space = datetime.timedelta(days=1)
    dayly_reminder.apply_async((user_id),
                               task_id=self.request.id, eta=self.request.eta + day_space, retry=False)
