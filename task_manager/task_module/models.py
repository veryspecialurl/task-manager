from django.db import models
from django.conf import settings
from django.utils.text import slugify
from task_manager.celery import app


class AbstractTask(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(allow_unicode=True, null=True)
    color = models.CharField(max_length=7, default='#660066')
    text = models.TextField(blank=True)
    date_making = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(f'{self.title}{self.date_making}', allow_unicode=True)
        super().save(*args, **kwargs)

    class Meta:
        abstract = True


class TimeTask(AbstractTask):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='time_tasks', null=True)
    date_success = models.DateField(null=True)
    time_success = models.TimeField(null=True)
    time_success2 = models.TimeField(null=True)

    def selery_serialize(self):
        dict_ = dict(
            title=self.title,
            text=self.text,
            date_success=self.date_success,
            time_success=self.time_success,
            time_success2=self.time_success2
        )
        return dict_

    def delete(self, *args, **kwargs):
        for c_task in CTask.objects.filter(task=self):
            c_task.revoke()
        super().delete(*args, **kwargs)


class CTask(models.Model):
    task = models.ForeignKey(TimeTask, on_delete=models.CASCADE, related_name='c_task')
    ctask_id = models.CharField(max_length=256)
    eta = models.CharField(max_length=20)

    def revoke(self):
        app.control.revoke(self.ctask_id, terminate=True)


class DaylyReminders(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, primary_key=True,
                                related_name='daily_reminder')
    ctask_id = models.CharField(max_length=256)
    eta = models.CharField(max_length=20)

    def delete(self, *args, **kwargs):
        app.control.revoke(self.ctask_id, terminate=True)
        super().delete(*args, **kwargs)

    def session_serialize(self):
        return dict(eta=self.eta)
