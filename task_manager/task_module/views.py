import json
import pytz
import logging

from celery.utils import gen_unique_id
from django.conf import settings
from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import View
from django.http import QueryDict
from django.core.exceptions import ObjectDoesNotExist

from task_manager.celery import app
from .backend import *
from .forms import NewTTask, DateInput, MakeReminder
from .models import CTask, DaylyReminders
from .tasks import send_reminder, dayly_reminder

file_logger = logging.getLogger('tm.file')


@method_decorator(login_required(login_url=reverse_lazy('auth:auth_page')), name='dispatch')
class MainView(View):
    """
    Just a main page of project.
    """

    def get(self, request, year):
        context = {
            'calendar': TTControl.render_calendar(year, request.user),
            'NewTTaskForm': NewTTask()
        }
        return render(request, 'task_module/main_page.html', context)


@method_decorator(login_required, name='dispatch')
class DayView(View):
    """
    View is responsible for all-task control.
    """

    def get(self, request):
        """
        Get all task from certain day.
        """
        # Get date from request
        year = request.GET.get('year', None)
        month = request.GET.get('month', None)
        day = request.GET.get('day', None)
        # Make tuple for create "fail' response
        data_list = ((year, 'year'), (month, 'month'), (day, 'day'))
        # Get name of empty param
        fields = (field[1] for field in data_list if field[0] is None)
        # Return response with unfilled params
        if any(item is None for item in (field[0] for field in data_list)):
            response = dict(message='Неправильный запрос', description=f'Не переданы поля: {",".join(fields)}')
            file_logger.error(f'{request.user.email} Не переданы поля: {",".join(fields)}')
            return HttpResponse(content=json.dumps(response), content_type='application/json', status=400)
        else:
            # Return task for date
            items = TTControl.get_task_list(request.user, datetime.date(year=int(year), month=int(month), day=int(day)))
            response = dict(items=items)
            return HttpResponse(content=json.dumps(response), content_type='application/json', status=200)

    def delete(self, request):
        """
        Delete all tasks or if request contain date param, delete all tasks before this date.
        """
        data = QueryDict(request.body)
        if data.get('date', None) is not None:
            # Delete all tasks before date
            TimeTask.objects.filter(user=request.user, date_success__lt=data['date']).delete()
            response = dict(message='Успешный запрос', description=f'Удалены все заметки до даты:{data["date"]}')
            return HttpResponse(content=json.dumps(response), content_type='application/json', status=200)
        else:
            # Delete all tasks
            TimeTask.objects.filter(user=request.user).delete()
            response = dict(message='Успешный запрос', description=f'Удалены все заметки')
            return HttpResponse(content=json.dumps(response), content_type='application/json', status=200)


@method_decorator(login_required, name='dispatch')
class TaskView(View):
    """
    View is responsible for task control.
    """

    def get(self, request, pk):
        """
        Get certain task.

        :param request:
        :param pk: task_id
        :return: json instance of task info or error response
        """
        # Get task or raise exception
        try:
            task = TimeTask.objects.get(pk=pk)
        except task.DoesNotExist:
            response = dict(message='Not found', description='Задача с таким id не найдена')
            return HttpResponse(content=json.dumps(response), content_type='application/json', status=404)
        # Make info dictionary
        item = dict(pk=task.id, title=task.title, color=task.color, text=task.text,
                    date=task.date_success.strftime('%Y|%m|%d'),
                    hour=task.time_success.hour, minute=task.time_success.minute, hour2=task.time_success2.hour,
                    minute2=task.time_success2.minute)
        response = dict(message='Успешный запрос', description='Запрос выполнен без ошибок', item=item)
        return HttpResponse(content=json.dumps(response), content_type='application/json', status=200)

    def post(self, request):
        """
        Create new task.

        :param request:
        :return: HTTP response (fail or success)
        """
        form = NewTTask(request.POST)
        if form.is_valid():
            # Parse date from string and create new task model
            date = datetime.datetime.strptime(form.cleaned_data['data'], '%Y|%m|%d')
            task = TimeTask.objects.create(user=request.user,
                                           title=form.cleaned_data['title'],
                                           text=form.cleaned_data['text'],
                                           color=form.cleaned_data['color'],
                                           date_success=date,
                                           time_success=form.cleaned_data['time_success'],
                                           time_success2=form.cleaned_data.get('time_success2', None))
            # Create dictionaries for response
            item = dict(
                pk=task.pk,
                title=task.title,
                text=task.text,
                color=task.color,
                hour=task.time_success.hour,
                minute=task.time_success.minute,
                hour2=task.time_success2.hour,
                minute2=task.time_success2.minute,
            )
            date = dict(
                year=task.date_success.year,
                month=task.date_success.month,
                day=task.date_success.day
            )
            response = dict(message='Успешный запрос', description='Запрос выполнен без ошибок', item=item, date=date)
            return HttpResponse(content=json.dumps(response), content_type='application/json', status=200)
        else:
            response = dict(message='Форма заполнена неверно', description='Форма заполнена неверно')
            return HttpResponse(content=json.dumps(response), content_type='application/json', status=400)

    def put(self, request, pk):
        """
        Update task.

        :param pk: task id
        :return: HTTP response (success or fail)
        """
        # Get task or raise exception
        try:
            task = TimeTask.objects.get(pk=pk)
        except task.DoesNotExist:
            response = dict(message='Not found', description='Задача с таким id не найдена')
            return HttpResponse(content=json.dumps(response), content_type='application/json', status=404)
        # Tell to the form, that we need to update task
        data = QueryDict(request.body)
        form = NewTTask(data, instance=task)
        # Update or send error response
        if form.is_valid():
            form.save()
            response = dict(message='Успешный запрос', description='Запрос выполнен без ошибок')
            return HttpResponse(content=json.dumps(response), content_type='application/json', status=200)
        else:
            response = dict(message='Форма заполнена неверно', description='Форма заполнена неверно')
            return HttpResponse(content=json.dumps(response), content_type='application/json', status=400)

    def delete(self, request, pk):
        """
        Delete task with id == pk.

        :param pk: task_id
        :return: HTTP response (success or fail)
        """
        # Get task or raise error
        try:
            task = TimeTask.objects.get(pk=pk)
        except task.DoesNotExist:
            response = dict(message='Not found', description='Задача с таким id не найдена')
            return HttpResponse(content=json.dumps(response), content_type='application/json', status=404)
        # Delete task
        task.delete()
        response = dict(message='Успешный запрос', description='Запрос выполнен без ошибок')
        return HttpResponse(content=json.dumps(response), content_type='application/json', status=200)


@method_decorator(login_required, name='dispatch')
class ReminderView(View):
    """
    ReminderView is responsible for accessing to reminders
    """

    def get(self, request, pk):
        """
        Return list of reminders, and vk-login status

        :param pk: task id
        :return: json with list of reminders dictionaries
        """
        # Generate list of reminders dictionaries
        data = [dict(pk=task.id, eta=task.eta) for task in CTask.objects.filter(task=pk)]
        return JsonResponse(dict(response=data, vk_connect=True if 'vk_credentials' in request.session else False))

    def post(self, request, pk):
        """
        Make new reminder.

        :param pk: task id
        :return: HTTP response (success or failed)
        """
        form = MakeReminder(request.POST)

        if form.is_valid():
            # Making datetime from str
            datetime_ = datetime.datetime.strptime(form.cleaned_data['date'] + '+' + form.cleaned_data['time'],
                                                   '%Y-%m-%d+%H:%M')
            # Get Local timezone and utc zone
            local_zone = pytz.timezone(settings.TIME_ZONE)
            # Convert to Utc
            localized_datetime_ = local_zone.localize(datetime_)
            new_datetime_ = localized_datetime_.astimezone(pytz.utc)
            # Get task or raise error
            try:
                task = TimeTask.objects.get(pk=pk)
            except task.DoesNotExist:
                response = dict(message='Not found', description='Задача с таким id не найдена')
                return HttpResponse(content=json.dumps(response), content_type='application/json', status=404)
            # Generate reminders id (celery task id) for saving its in model
            ctask_id = gen_unique_id()
            # Create celery task with generated task_id
            send_reminder.apply_async((request.session['vk_credentials']['user_id'], pk),
                                      task_id=ctask_id,  retry=False)
            # Save reminders model
            new_task = CTask.objects.create(task=task, ctask_id=ctask_id, eta=datetime_.strftime('%Y-%m-%dT%H:%M:%SZ'))
            # send response
            response = dict(message='Успешный запрос', description='Запрос выполнен без ошибок',
                            data=dict(pk=new_task.id, eta=datetime_.strftime('%Y-%m-%dT%H:%M:%SZ')))
            return HttpResponse(content=json.dumps(response), content_type='application/json', status=200)
        else:
            response = dict(message='Форма заполнена неверно', description='Форма заполнена неверно')
            return HttpResponse(content=json.dumps(response), content_type='application/json', status=400)

    def delete(self, request, pk):
        """
        Delete all reminders from task with primary key = pk.

        :param pk: task id
        """
        data = QueryDict(request.body)
        # If request contains key "rem_id" will be deleted only reminder with ctask_id == rem_id
        # Else delete all reminders of task
        if 'rem_id' in data:
            try:
                task = CTask.objects.get(pk=int(data['rem_id']))
                task.revoke()
                task.delete()
            except task.DoesNotExist:
                response = dict(message='Not found', description='Напоминание с таким id не найдено')
                return HttpResponse(content=json.dumps(response), content_type='application/json', status=404)
        else:
            reminders_ids = [reminder.ctask_id for reminder in CTask.objects.filter(task=pk)]
            app.control.revoke(reminders_ids, terminate=True)
        response = dict(message='Успешный запрос', description='Запрос выполнен без ошибок')
        return HttpResponse(content=json.dumps(response), content_type='application/json', status=200)


@method_decorator(login_required, name='dispatch')
class SettingsView(View):
    """
    Just settings page.
    """

    def get(self, request):
        context = {
            'form': DateInput(),
            'vk': request.session.get('vk_credentials', None)
        }
        return render(request, 'task_module/setting_page.html', context)


class BotUtils(View):
    def get(self, request):
        return render(request, 'task_module/bot_page.html')


class DaylyReminder(View):
    def get(self, request):
        """
        Returns info about daily reminder if exist, else return 404 error
        """
        try:
            daily_reminder = DaylyReminders.objects.get(user=request.user)
            response = dict(message='Успешный запрос', description='Запрос выполнен без ошибок',
                            data=dict(eta=daily_reminder.eta))
            return HttpResponse(content=json.dumps(response), content_type='application/json', status=200)
        except ObjectDoesNotExist:
            response = dict(message='Not found', description='Напоминание не найдено')
            return HttpResponse(content=json.dumps(response), content_type='application/json', status=404)

    def post(self, request):
        """
        Make daily reminder about tasks
        """
        data = request.POST
        # Return error if no time param
        if data.get('time', None) is None:
            response = dict(message='Форма заполнена неверно', description='Форма заполнена неверно')
            return HttpResponse(content=json.dumps(response), content_type='application/json', status=400)
        # Get current date
        current_date = datetime.date.today()
        # Get time from post request
        time = datetime.time(hour=int(data['time'][:2]), minute=int(data['time'][-2:]))
        # Get local zone
        local_zone = pytz.timezone(settings.TIME_ZONE)
        # Combining datetime and convert to utc tz
        localized_datetime_ = local_zone.localize(datetime.datetime.combine(current_date, time))
        new_datetime = localized_datetime_.astimezone(pytz.utc)
        # Add 1 day to datetime
        day_space = datetime.timedelta(days=1)
        # Generate task id
        ctask_id = gen_unique_id()
        dayly_reminder.apply_async((request.user),
                                   task_id=ctask_id, eta=new_datetime + day_space, retry=False)
        DaylyReminders.objects.create(user_id=request.user, ctask_id=ctask_id, eta=time.strftime('%H:%M'))
        response = dict(message='Успешный запрос', description='Запрос выполнен без ошибок')
        return HttpResponse(content=json.dumps(response), content_type='application/json', status=200)
