

var CONTEXTMENU_STANDART = {
    'menu': {
      'z-index':'200',
      'position':'absolute',
      'background-color': 'white',
    },
    'button': {
      //only top
      'padding':'6px 0'
    },
    'text': {
      //only sides
      'padding':'0 20px',
    }
  }

  export class Contextmenu {

    constructor(x, y) {
      Contextmenu.close();
      this.buttons = [];
      this.menu = $('<div>', {'class':'context-menu-js-class-identificator d-flex flex-column'}).appendTo('.main');
      this.menu.css({
        left:x,
        top:y,
      });
      this.menu.css(CONTEXTMENU_STANDART['menu']);
    }

    add_button(text, kwargs=null){
      var button = new Button(this.menu, text, kwargs);
      return button
    }

    static close(){
      $('.context-menu-js-class-identificator').remove()
    }

    static add_close_event(){
      $(document).click(function(event) {
        if (!$(event.target).is('.context-menu-js-class-identificator')) {
          Contextmenu.close();
        }
      });
    }

  }



  export class Button {

      constructor(menu, text, kwargs=null) {
        this.kwargs = kwargs;
        this.menu = menu;
        this.primary_menu = false;
        this.button = $('<div>', {'class':'context-menu-js-class-identificator-button'}).appendTo(menu);
        this.button.css(CONTEXTMENU_STANDART['button']);
        this.text = $('<span>').text(text).appendTo(this.button);
        this.text.css(CONTEXTMENU_STANDART['text']);
        this.color_events(this);
      }

      add_button(text, kwargs=null){
        if (this.primary_menu == false) {
          this.primary_menu = $('<div>', {'class':''}).appendTo(this.button)
          this.primary_menu.css({
            'display':'none',
          });
          this.primary_menu.css(CONTEXTMENU_STANDART['menu']);
          this.load_events(this);
        }
        var button = new Button(this.primary_menu, text, kwargs);
        return button
      }

      add_click_event(func) {
        this.button.click(function(event) {
          func();
        });
      }

      color_events(class_) {

        this.button.mouseenter(function() {
          class_.button.css({'background-color':'#00bfff'});
        });

        this.button.mouseleave(function() {
          class_.button.css({'background-color':'white'});
        });
      }

      load_events(class_) {

        this.button.mouseenter(function() {
          var pos = class_.button.position();
          class_.primary_menu.css({
            'display':'block',
            left:class_.button.width(),
            top:pos.top,
          });
        });

        this.button.mouseleave(function() {
          class_.primary_menu.css({
            'display':'None',
          });
        });

      }

    }