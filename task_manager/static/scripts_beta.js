function show_popup_window() {
  $('.popup_window').css({
    left: ($(window).width() - $('.popup_window').width()) / 2,
    top: $('html').offset().top + ($(window).height() - $('.popup_window').height()) / 2
  })
  $('.popup_window').show('fast');
}

function close_popup_window() {
  $('.popup_window').hide('fast');
  //$( ".popup_window" ).empty();
}

function load_timeline() {
  timeline = create_time_line($('.foo'), 12);
}

function create_time_line(insert_to, number) {
  var procent = 100 / (number);
  var time_line = $('.time-line').clone();
  time_line.css({display:'block'});
  time_line.appendTo(insert_to);
  $('<div>', {
    class: 'line'
  }).appendTo(time_line);
  for (let i = 0; i < number+1; i++) {
    var circle = $('<div>', {
      class: 'circle'
    }).appendTo('.line');
    var offset = (i * procent).toString() + '%';
    circle.css({
      'left': offset,
      'top': '-2px'
    });
    $('<div>', {
      class: 'time-line-value',
      id: 'time-line-value' + i,
      text: i.toString()
    }).appendTo(circle);
  }
  return time_line
}
