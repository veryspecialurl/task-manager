import {
	Contextmenu, Button
}
from './contextmenu.js';
import {
	TimeLine
}
from './timeline.js'
import {
	create_popup_message, create_choose_window, popup_window
}
from './popup_windows.js'

import {
	Time,
	DateU
}
from './utils.js'


// CSRF_TOKEN FIX
$.ajaxSetup({
	beforeSend: function(xhr, settings) {
		function getCookie(name) {
			var cookieValue = null;
			if (document.cookie && document.cookie != '') {
				var cookies = document.cookie.split(';');
				for (var i = 0; i < cookies.length; i++) {
					var cookie = jQuery.trim(cookies[i]);
					// Does this cookie string begin with the name we want?
					if (cookie.substring(0, name.length + 1) == (name + '=')) {
						cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
						break;
					}
				}
			}
			return cookieValue;
		}
		if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
			// Only send the token to relative URLs i.e. locally.
			xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
		}
	}
});

// GLOBAL VALUES
var gradients_endpoints = [];

function load_ttasks(date, reload=false) {
	/*
		Load all tasks for current date,
		reload arg says that window must be open momently without animation
	*/
	var year = date.getFullYear();
	// Fix month
	var month = date.getMonth()+1;
	var day = date.getDate();

	$.getJSON('/day/', {
			year: year,
			month: month,
			day: day
		}, function(json, textStatus) {})
		.fail(function(data) {
			create_popup_message('Oops something had gone wrong...');
		})
		.done(function(data) {
			
			// Creating time-line instance
			var daytm = new TimeLine(24);
			daytm.create_time_line();

			// Iteratint through received gradients
			$.each(data.items, function(index, item) {
				var start_time = new Time(item.hour, item.minute);
				var end_time = new Time(item.hour2, item.minute2);
				if (item.hour == item.hour2) {
					end_time = null
				}
				daytm.add_gradient(item.title,
					start_time,
					end_time,
					item.color, {
						'title': item.title,
						'text': item.text,
						'hour': item.hour,
						'minute': item.minute,
						'hour2': item.hour2,
						'minute2': item.minute2
					},
					item.pk)
				});

			// Creating window instance
			var windows_instance = new popup_window('Задачи на этот день', null, '80%');

			// Body of window
			var windows = windows_instance.body

			// Filling body
			var div = $('<div>', {
				'class': 'd-flex flex-row'
			}).appendTo(windows);

			// Add new task form
			daytm.form = $('.newttaskform').first().clone().appendTo(div);

			// Timeline append to window body
			$(daytm.time_line).appendTo(div);
			
			// Set current date
			daytm.form.children('form').children('#data-date').val(year.toString() + '|' + month.toString() + '|' + day.toString());

			// Showing window
			if(reload == true){
				windows_instance.fast_show();
			} else {
				windows_instance.show();
			}
			const CURRENT_DATE = new Date();
			
			// Timeline events
			// Works if current date < date of task
			if (DateU.compare(CURRENT_DATE, date) == -1 || DateU.compare(CURRENT_DATE, date) == 0) {

				// add current line
				daytm.add_current_line();
				var block = $('<div>', {id:'current_block'})
				block.css(daytm.STANDART_CSS['gradient-box']);
				

				TimeLine.change_current_line_color(daytm.current_line, daytm.form.find('input[type=color]').val());
				TimeLine.change_current_line_color(block, daytm.form.find('input[type=color]').val());

				var left = null;
				var button_down = false;
				var available_values = null;
				/*
					Making selectable area 
				*/
				
				reload_gradients_endpoints_endpoints();
				
				// Change color selector when change color value
				daytm.form.find('input[type=color]').change(function() {
					TimeLine.change_current_line_color(block, $(this).val());
					TimeLine.change_current_line_color(daytm.current_line, $(this).val());
				});

				$(daytm.time_line).mousedown(function(event) {
					if (event.button === 2){
						return ;
					}
					button_down = true;
					block.appendTo(daytm.time_line);
					if ($(event.target).is('.gradient')) {
						block.css({
							'display':'block',
							'left': parseInt($(event.target).css('left').slice(0, -2))+$(event.target).width()+1,
							'width': '2px'
						});
						available_values = between_in_array(parseInt($(event.target).css('left').slice(0, -2))+$(event.target).width()+1, gradients_endpoints);
						left = parseInt($(event.target).css('left').slice(0, -2))+$(event.target).width()+1+$(this).offset().left;
					} else {
						block.css({
							'display':'block',
							'left': event.pageX - $(this).offset().left,
							'width': '2px'
						});
						available_values = between_in_array(event.pageX - $(this).offset().left, gradients_endpoints);
						left = event.pageX;
					}
				});
				$(daytm.time_line).mousemove(function(event) {
					if (button_down == true) {
						if (event.pageX - $(this).offset().left > daytm.time_line.width()) {
							return;
						}
						var right = parseInt(block.css('left').slice(0, -2))+event.pageX - left;
						if (available_values != null && right > available_values[1]) {
							block.css({
								'width': available_values[1] - block.css('left').slice(0, -2)
							})
						} else {
							block.css({
								'width': event.pageX - left
							});
						}
						var block_start = parseInt(block.css('left').slice(0, -2));
						var block_end = block_start + block.width();
						var block_start_time = TimeLine.pos_to_time(block_start, daytm.time_line.width());
						var block_end_time = TimeLine.pos_to_time(block_end, daytm.time_line.width());
						
						$('.timesuccess').val(block_start_time);
						if (block_end_time == '23:60') {
							$('.timesuccess2').val('23:59');
						} else {
							$('.timesuccess2').val(block_end_time);
						}
					}
				});

				$(document).mouseup(function(event) {
					button_down = false;
				});

				$(daytm.time_line).click(function(event) {
					if ($('.timesuccess').val() == daytm.current_line_data.text()) {
						$('.timesuccess2').val(daytm.current_line_data.text())
						button_down = false;
						block.css({
							'width': '2px'
						});
					}
				});

			} else {

				// Block form if current date > task date
				daytm.form.find('input').prop({disabled: 'True'});
				daytm.form.find('submit').prop({disabled: 'True'});
				daytm.form.find('textarea').prop({disabled: 'True'});
			}
			$('.time-line').on( "contextmenu", ".gradient", function(evt) {
				evt.preventDefault();
				//------------------------------------------------------------------контекстное меню-----------------------------------------------------//
				var context_menu = new Contextmenu(evt.pageX, evt.pageY);
				var change_button = context_menu.add_button('Редактировать', {
					'data-id': $(this).attr('id'),
					'data-year': year,
					'data-month': month,
					'data-day': day
				});
				var delete_button = context_menu.add_button('Удалить', {
					'data-id': $(this).attr('id'),
					'data-year': year,
					'data-month': month,
					'data-day': day
				});
				var send_vk_button = context_menu.add_button('Напоминания',
				{
					'pk': $(this).attr('data-pk'),
					'year': year,
					'month': month,
					'day': day,
					'start_time':$(this).attr('data-start'),
					'task-id':$(this).attr('id')
				});
				// Events -----------------------------------------------------------------------------------------------------------------------------------//
				change_button.add_click_event(function() {
					var gradient = $(document.getElementById(change_button.kwargs['data-id'])).clone()
					var windows_instance = new popup_window('Редактирование задачи', null, '80%');
					var windows = windows_instance.body
					
					var div = $('<div>', {
						'class': 'd-flex flex-row'
					}).appendTo(windows)
					var update_timeline = new TimeLine(24);
					update_timeline.form = $('.newttaskform').clone().first().appendTo(div);
					update_timeline.create_time_line();
					gradient.appendTo(update_timeline.time_line);
					$(update_timeline.time_line).appendTo(div);
					$.getJSON('/task/' + gradient.attr('data-pk'), function(json, textStatus) {})
						.fail(function(data) {
							return null;
						})
						.done(function(data) {
							var item = data.item;
							update_timeline.form.attr('action', '/task/' + gradient.attr('data-pk'));
							update_timeline.form.find('[name = title]').attr('placeholder', 'Изменить заголовок');
							update_timeline.form.find('[name = title]').val(item.title);
							update_timeline.form.find('[name = color]').val(item.color);
							update_timeline.form.find('[name = time_success]').val(TimeLine.normalize_time(item.hour) + ':' + TimeLine.normalize_time(item.minute));
							update_timeline.form.find('[name = time_success2]').val(TimeLine.normalize_time(item.hour2) + ':' + TimeLine.normalize_time(item.minute2));
							update_timeline.form.find('[name = time_success2]').val(TimeLine.normalize_time(item.hour2) + ':' + TimeLine.normalize_time(item.minute2));
							update_timeline.form.find('[name = data]').val(item.date);
							update_timeline.form.find('[type = submit]').val('Изменить');
							update_timeline.form.submit(function(event) {
								var date = item.date.split('|')
								update_task(gradient.attr('data-pk'), update_timeline, update_timeline.form.find('form'), date[0], date[1], date[2]);
								event.preventDefault();
							});
						});
					windows_instance.show();
				});
				delete_button.add_click_event(function() {
					var gradient = $(document.getElementById(change_button.kwargs['data-id']));
					create_choose_window('Вы действительно хотите удалить эту задачу?', function() {
						delete_gradient(gradient, change_button.kwargs['data-year'], change_button.kwargs['data-month'], change_button.kwargs['data-day'])
					});
				});
				send_vk_button.add_click_event(function() {
					const curent_date = new Date();
					var task_date = new Date(parseInt(send_vk_button.kwargs['year']), parseInt(send_vk_button.kwargs['month'])-1,parseInt(send_vk_button.kwargs['day']));
					
					var task = $(document.getElementById(send_vk_button.kwargs['task-id']));

					var task_time_start = Time.strptime(task.attr('data-start'), '%H:%M');
					var task_time_end = Time.strptime(task.attr('data-end'), '%H:%M');
					var current_time = Time.fromDateInstance(curent_date);

					curent_date.setDate(curent_date.getDate()-1);
					if (curent_date.getTime() > task_date.getTime()) {
						return create_popup_message('Нельзя добавлять напоминания в старые задачи', 'danger', 1500);
					}
					var windows_instance = new popup_window(`Напоминания ВКонтакте ${DateU.strfdate(task_date, '| %dh %D %mr')}`, null, '80%');
					var windows = windows_instance.body;
					var form = $('#create_reminder').clone().appendTo(windows);
					windows_instance.show();
					$.getJSON('/reminder/'+send_vk_button.kwargs['pk']).done(function(data){
						if (data['vk_connect'] == false) {
							create_popup_message('У вас не привязан аккаунт ВКонтакте, вы не можете добавлять новые заметки');
							form.find('input').prop('disabled', true);
							form.find('submit').prop('disabled', true);
							form.find('button').prop('disabled', true);
						}
						$.each(data['response'], function(index, reminder) {

							var tr = $('<tr>', {id:'tr'+reminder['pk']}).appendTo(windows.find('#reminders_list'));

				            var date = DateU.strpdate(reminder['eta'].slice(0, 10), "%Y-%m-%d");
				            var time = Time.strptime(reminder['eta'].slice(10), 'T%H:%M:%SZ');

							$('<td>', {class:'text-center', text:DateU.strfdate(date, '%D %mr')}).appendTo(tr);
							$('<td>', {class:'text-center', text:Time.strftime(time, '%h:%m')}).appendTo(tr);
				            let close = $('<td>', {class:'text-center'}).appendTo(tr);
				            $('<a>', {href:'#', 'data-pk':reminder['pk'], class:'delete-reminder'}).text('X').appendTo(close);

						});
						windows.on( "click", ".delete-reminder", function(event) {
			              event.preventDefault();
			              delete_reminder(send_vk_button.kwargs['pk'], $(this).attr('data-pk'), $(this).parent().parent());
			            });	
					})
          
					form.find('input#rem_date_input').change(function() {
						var list_date = $(this).val().split('-')
						var val_date = new Date(parseInt(list_date[0]), parseInt(list_date[1])-1, parseInt(list_date[2]));
						if (val_date.getTime() > task_date.getTime() || val_date.getTime() < curent_date.getTime()) {
							$(this).addClass('is-invalid');
							$(this).removeClass('is-valid');
							form.find('input[type=submit]').prop( "disabled", true );						
						} else {
							$(this).addClass('is-valid');
							$(this).removeClass('is-invalid');
							if ($('input#rem_date_input').hasClass('is-valid') && $('input#rem_time_input').hasClass('is-valid')){
								form.find('input[type=submit]').prop( "disabled", false );
							}
						}
						form.find('input#rem_time_input').trigger('change');
					});
					form.find('input#rem_time_input').change(function() {
						var list_date = $(this).val().split(':')
						var val_time = new Time(parseInt(list_date[0]), parseInt(list_date[1]));
						var date = form.find('input#rem_date_input').val().split('-')
						var val_date = new Date(parseInt(date[0]), parseInt(date[1])-1, parseInt(date[2]));
						if (val_date.getFullYear() != task_date.getFullYear() || val_date.getMonth() != task_date.getMonth() || val_date.getDate() != task_date.getDate()) {
							$(this).addClass('is-valid');
							$(this).removeClass('is-invalid');
							if ($('input#rem_date_input').hasClass('is-valid') && $('input#rem_time_input').hasClass('is-valid')){
								form.find('input[type=submit]').prop( "disabled", false );
							}
							return;
						}
						if (Time.compare(task_time_end, val_time) == -1) {
							$(this).addClass('is-invalid');
							$(this).removeClass('is-valid');
							form.find('input[type=submit]').prop( "disabled", true );						
						} else {
							$(this).addClass('is-valid');
							$(this).removeClass('is-invalid');
							if ($('input#rem_date_input').hasClass('is-valid') && $('input#rem_time_input').hasClass('is-valid')){
								form.find('input[type=submit]').prop( "disabled", false );
							}
						}
					});
					form.submit(function(event) {
						event.preventDefault();
						create_reminder(0, 0, task_time_start, task_time_end, curent_date, task_date, form, send_vk_button, current_time, form.find('form').serialize());	
					});
					form.find('#add_reminder_1_hour').click(function(event) {
						event.preventDefault();
						create_reminder(0, 1, task_time_start, task_time_end, curent_date, task_date, form, send_vk_button, current_time, null);
					});
					form.find('#add_reminder_3_hour').click(function(event) {
						event.preventDefault();
						create_reminder(0, 3, task_time_start, task_time_end, curent_date, task_date, form, send_vk_button, current_time, null);
					});
					form.find('#add_reminder_1_day').click(function(event) {
						event.preventDefault();
						create_reminder(1, 0, task_time_start, task_time_end, curent_date, task_date, form, send_vk_button, current_time, null);
					});
					form.find('#add_reminder_3_day').click(function(event) {
						event.preventDefault();
						create_reminder(3, 0, task_time_start, task_time_end, curent_date, task_date, form, send_vk_button, current_time, null);
					});
				});
			});
			// Events -----------------------------------------------------------------------------------------------------------------------------------//
			Contextmenu.add_close_event();
			//------------------------------------------------------------------контекстное меню-----------------------------------------------------//
			$('.time-line').on( "mouseenter", ".gradient", function(event) {
				daytm.current_line_data.css({
					'display': 'None'
				});
				daytm.current_line.css({
					'display': 'None'
				});
				var gr = daytm.gradients[$(this).attr('id')];
				var grinfo = $('<div>', {
					class: 'gr-info shadow'
				});
				$('<p>').text(`${gr['title']}`).appendTo(grinfo)
				$('<p>').text(`${gr['text']}`).appendTo(grinfo)
				if (`${gr['hour']}:${gr['minute']}` != `${gr['hour2']}:${gr['minute2']}`) {
					$('<p>').text(`${gr['hour']}:${gr['minute']}-${gr['hour2']}:${gr['minute2']}`).appendTo(grinfo)
				} else {
					$('<p>').text(`${gr['hour']}:${gr['minute']}`).appendTo(grinfo)
				}
				grinfo.css(daytm.STANDART_CSS['grinfo'])
				grinfo.appendTo(daytm.time_line)
			});
			$('.time-line').on( "mouseleave", ".gradient", function(event) {
				daytm.current_line.css({
					'display': 'block'
				});
				daytm.current_line_data.css({
					'display': 'block'
				});
				$('.gr-info').remove();
			});
			daytm.form.children('form').submit(function(event) {
				create_task(daytm, daytm.form.children('form'));
				event.preventDefault();
			});
		});
	}

function create_task(time_line, form) {
	$.ajax({
			url: '/task/',
			type: 'POST',
			data: form.serialize(),
		})
		.done(function(data) {
			var item = data.item
			var start_time = new Time(item.hour, item.minute);
			var end_time = new Time(item.hour2, item.minute2);
			if (item.hour == item.hour2) {
					end_time = null
				}
			time_line.add_gradient(item.title,
				start_time,
				end_time,
				item.color, {
					'title': item.title,
					'text': item.text,
					'hour': item.hour,
					'minute': item.minute,
					'hour2': item.hour2,
					'minute2': item.minute2
				},
				item.pk)
			var task_date = new Date(parseInt(data.date.year), parseInt(data.date.month), parseInt(data.date.day));
			var day = $(`td[data-year=${data.date.year}][data-month=${data.date.month}][data-day=${data.date.day}]`)
			if (day.children().length == '0') {
				day.append("<div class='task-count'><span>1</span></div>");
			} else {
				var tasks_number = (parseInt(day.children('.task-count').children('span').text()) + 1).toString();
				day.children('.task-count').children('span').text(tasks_number);
			}
			$('#current_block').css({'display':'none'});
			reload_gradients_endpoints_endpoints();
			create_popup_message('Задача успешно создана', 'success', '1000')
		})
		.fail(function(data) {
			alert(data.message)
		})
}

function update_task(pk, time_line, form, year, month, day) {
	$.ajax({
			url: '/task/' + pk,
			type: 'PUT',
			data: form.serialize(),
		})
		.done(function() {
			create_popup_message('Заметка успешно изменена');
			reload_gradients_endpoints_endpoints();
		})
		.fail(function(data) {
			alert(data.description)
		})
}

function delete_gradient(elem, year, month, days) {
	$.ajax({
			url: '/task/' + elem.attr('data-pk'),
			type: 'DELETE',
		})
		.done(function() {
			elem.remove();
			var day = $(`td[data-year=${year}][data-month=${month}][data-day=${days}]`);
			var tasks_count = parseInt(day.children('.task-count').children('span').text())
			if (tasks_count == 1) {
				day.children('.task-count').children('span').remove()
			} else {
				day.children('.task-count').children('span').text((tasks_count - 1).toString());
			}
			reload_gradients_endpoints_endpoints();
		})
		.fail(function() {
			create_popup_message('При удаление заметки произошла ошибка');
		})
}


function load_task(pk, year, month, day) {
	$.getJSON('/task/' + pk, function(json, textStatus) {})
		.fail(function(data) {
			return null;
		})
		.done(function(data) {
			return data.item;
		});
}

$(document).ready(function() {
	$(document).mousedown(function(event) {
		if ($(event.target).is('.context-menu-elem') && $(event.target).text() == 'Удалить') {
			var gradient = $(document.getElementById($(event.target).attr('data-id')));
			create_choose_window('Вы действительно хотите удалить эту задачу?', function() {
				delete_gradient(gradient, $(event.target).attr('data-year'), $(event.target).attr('data-month'), $(event.target).attr('data-day'))
			});
			Contextmenu.close();
		}
	});
	$('.nblank-cell').click(function(event) {
		load_ttasks(new Date(parseInt($(this).attr('data-year')), parseInt($(this).attr('data-month')-1), parseInt($(this).attr('data-day'))));
	});
	$('#delete_all_tasks').click(function() {
		delete_all_tasks();
	});
	$('#delete_all_tasks_before').click(function() {
		delete_all_tasks_before();
	});
	$('#add_dayly_reminder').click(function(){
	    $.ajax({
					url: 'create_reminder',
					type: 'GET'
				})
				.done(function() {
				    create_popup_message('Измените задачу');
				})
				.fail(function() {
					var form = null
                    create_choose_window('Выберите время в которое вы будее получать уведомления', function() {
                        if (form.find('input[name=time]').val() == '') {
                            return;
                        }
                        $.ajax({
                                url: 'create_reminder',
                                type: 'POST',
                                data: form.serialize()
                            })
                            .done(function() {
                                create_popup_message('Теперь каждое утро вы будете получать уведомления о всех задачах на день');
                            })
                            .fail(function() {
                                console.log("error");
                            })
                        },
                        function() {
                            form = $('#reminder_form').clone().appendTo('.popup_choose');
                        }
                    );
				})
	});
});

function delete_all_tasks() {
	create_choose_window('Вы действительно хотите удалить все задачи?', function() {
		$.ajax({
			url: '/day/',
			type: 'DELETE',
		})
		.done(function(data) {
			create_popup_message(data.description);
		})
		.fail(function() {
			console.log("error");
		})
	})
}

function delete_all_tasks_before() {
	var form = null
	create_choose_window('Выберите дату до которой необходимо удлать все задачи:', function() {
        $.ajax({
                url: '/day/',
                type: 'DELETE',
                data: form.serialize()
            })
            .done(function() {
                create_popup_message(`Заметки до даты [${form.find('[name=date]').val()}] успешно удалены`).appendTo('.popup_message');
            })
            .fail(function() {
                console.log("error");
            })
		},
		function() {
			form = $('.date-form').clone().appendTo('.popup_choose');
		}
	);
}

function delete_reminder(pk, rem_id=null, tr) {
	if (rem_id != null) {
		var data_ = {
			'rem_id':rem_id
		}
	} else {
		var data_ = {}
	}
	$.ajax({
		url: '/reminder/'+pk,
		type: 'DELETE',
		data: data_
	})
	.done(function(data) {
		tr.remove();
		create_popup_message('Заметка успешно удалена');
	})
	.fail(function(data) {
		create_popup_message(data['message']);
	})			
	
}

function create_reminder(days, hour, task_time_start, task_time_end, curent_date, task_date, form, send_vk_button, current_time, data=null) {
	// copying time and date of task
  if (data == null) {
  	var new_time = new Time(task_time_start.hour, task_time_start.minute);
  	var task_date_ = DateU.copy(task_date);
  	// change time and date 
  	task_date_.addDay(-1*days);
  	new_time.addHour(-hour);

  	//cheking that current datetime > task datetime
  	if(curent_date.getTime() > task_date_.getTime()){
  		create_popup_message(`До начала задачи осталось меньше [${days}] дня(ей)`);
  		return;
  	}
  	if(DateU.is(curent_date, task_date) && Time.compare(new_time, current_time) == -1){
  		create_popup_message(`До начала задачи осталось меньше [${hour}] часа(ов)`);
  		return;
  	}
  	var data_ = {
  		csrfmiddlewaretoken:form.find('input[name=csrfmiddlewaretoken]').val(),
  		date:DateU.strfdate(task_date_, '%Y-%m-%d'),
  		time:Time.strftime(new_time, '%h:%m')
  	}
  } else {
    var data_ = data;
  }
	$.ajax({
		url: '/reminder/'+send_vk_button.kwargs['pk'],
		type: 'POST',
		data: data_
	})
	.done(function(data) {
		create_popup_message('Заметка успешно дабавлена');

		var tr = $('<tr>', {id:'tr'+data['data']['pk']}).appendTo(form.find('#reminders_list'));

        var date = DateU.strpdate(data['data']['eta'].slice(0, 10), "%Y-%m-%d");
        var time = Time.strptime(data['data']['eta'].slice(10), 'T%H:%M:%SZ');

		$('<td>', {class:'text-center', text:DateU.strfdate(date, '%D %mr')}).appendTo(tr);
		$('<td>', {class:'text-center', text:Time.strftime(time, '%h:%m')}).appendTo(tr);
        let close = $('<td>', {class:'text-center'}).appendTo(tr);
        $('<a>', {href:'#', 'data-pk':data['data']['pk'], class:'delete-reminder'}).text('X').appendTo(close);
	})
	.fail(function(data) {
		create_popup_message(data['message']);
	})			
}

function between_in_array(number, array) {
	var return_ = null;
	if (number < array[0]){
		return_ = [0, array[0]];
	}
	for (var i = 0; i < array.length; i++) {
		if (array[i] < number && number < array[i+1]) {
			return_ = [array[i], array[i+1]];
			break;
		}
	}
	return return_
}

function is_between(number, array) {
	if (number > array[0] && number < array[1]) {
		return true;
	} else {
		return false;
	}
}

function reload_gradients_endpoints_endpoints() {
	gradients_endpoints = []
	$('.gradient').each(function(index, el) {
		let left = parseInt($(el).css('left').slice(0, -2));
		let width = parseInt($(el).css('width').slice(0, -2))
		gradients_endpoints.push(left);
		gradients_endpoints.push(left+width);
	});
	gradients_endpoints.sort(function(a, b){
		return a - b;
	})
}