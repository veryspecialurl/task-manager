import {
  Time
}
from './utils.js'

// TIMELINE MODULE
var STANDART_VALUES = {
  'bottom':'0%'
}
var STANDART_CSS = {
  'time-line': {
    'width':'80%',
  	'position': 'relative',
  	'background-color': '#E8E8E8',
    'margin':'20px',
    'outline':'20px solid #E8E8E8'
  },
  'line': {
  	'height': "2px",
  	"background-color": "black",
  	"width": '100%',
  	"position": "absolute",
  	'margin': 'auto',
  	'left': '0%',
  	'bottom': STANDART_VALUES['bottom'],
    'z-index':'4'
  },
  'circle': {
  	'background-color': '#000000',
  	'border-radius': '50%',
  	'width': '6px',
  	'height': '6px',
  	'position': 'absolute',
  	'top': '-2px',
    'margin':' 0 -3px'
  },
  'line-values': {
  	'position': 'absolute',
  	'top': '-20px',
  	'font-size': '12px'
  },
  'current-line': {
  	'width': '2px',
  	'height': '100%',
  	'background-color': '#F79EB1',
  	'position': 'absolute',
  	'bottom': '0',
    'padding':'0',
    'margin':'0'
  },
  'current-line-data': {
  },
  'gradient-box': {
    'width': '1px',
    'height': '100%',
    'position': 'absolute',
    'bottom': STANDART_VALUES['bottom'],
    'padding':'0',
    'margin':'0',
    'background': 'linear-gradient(0deg, rgba(247,158,177,1) 0%, rgba(0,212,255,0) 80%)',
    'z-index':'1',
  },
  'grinfo':{
    "background-color": "white",
    "position": "absolute",
    'left': '0px',
    'top': '5px',
    'z-index':'4',
    'padding':'10px'
  }
}

export class TimeLine {
  constructor(points) {
  	this.points_count = points;
  	this.points = [];
    this.gradients = {};
  	this.STANDART_CSS = STANDART_CSS;
    this.starttime = null;
    this.endtime = null;
    this.info_block = null;
    this.form = null;
  }

  create_time_line() {
  	this.procent = 100 / (this.points_count);
  	this.time_line = $('<div>', {
  	  class: 'time-line',
      onselectstart:"return false"
  	});
  	this.time_line.css(this.STANDART_CSS['time-line']);
  	this.line = $('<div>', {
  	  class: 'line'
  	}).appendTo(this.time_line);
  	this.line.css(this.STANDART_CSS['line']);

  	for (let i = 0; i < this.points_count + 1; i++) {
  	  var circle = $('<div>', {
  		class: 'circle'
  	  }).appendTo(this.line);
  	  var offset = (i * this.procent).toString() + '%';
  	  var css_circle = this.STANDART_CSS['circle'];
  	  css_circle['left'] = offset;
  	  circle.css(css_circle);
  	  var value = $('<div>', {
  		class: 'time-line-value',
  		id: 'time-line-value' + i,
  		text: i.toString()
  	  });
  	  value.css(this.STANDART_CSS['line-values']);
  	  value.appendTo(circle);
  	}
  }

  add_point(name, where, color='#000000', data=null) {
  	//especialy take where values in %
  	//ДОабвить проверку на наличие в переменной точки
    	var circle = $('<div>', {
    	  class: 'circle',
    	  id: name
    	}).appendTo(this.line);
    	this.points.push({
    	  'name': name,
    	  'object': circle,
        'data': data
    	})
    	circle.css(this.STANDART_CSS['circle']);
      circle.css({
        'left':(where).toString() + '%',
        'background-color':color
      });
      return circle;
    }

  add_gradient(name, start, end=null, color='#000000', data=null, pk) {
    var start_in_procent = TimeLine.procent(start.in_minutes(), 24*60);
    
    var gradient = $('<div>', {
      class:'gradient',
      id:'gr'+name+(start_in_procent).toString(),
      'data-pk':pk,
      'data-start':Time.strftime(start, '%H:%M'),
    }).appendTo(this.time_line);
    if (end != null) { 
      var end_in_procent = TimeLine.procent(end.in_minutes(), 24*60);
      gradient.attr({
        'data-end': Time.strftime(end, '%H:%M')
      });
    }
    this.gradients['gr'+name+(start_in_procent).toString()] = data;
    gradient.css(this.STANDART_CSS['gradient-box']);
    color = TimeLine.convertHex(color, 50);
    if(end != null){
      gradient.css({
        'left':(start_in_procent).toString() + '%',
        'width':`calc(${(end_in_procent-start_in_procent).toString()}% + 2px)`,
        'background': `linear-gradient(0deg, ${color} 0%, rgba(0,212,255,0) 80%)`
      });
    } else {
      gradient.css({
        'left':(start_in_procent).toString() + '%',
        'width':'2px',
        'background': `linear-gradient(0deg, ${color} 0%, rgba(0,212,255,0) 80%)`
      });
    }
    return gradient
  }

  change_width(procent) {
  	//change line width in procent
  	this.line.css({
  	  "width": procent + '%',
  	  'left': ((100 - procent) / 2).toString() + '%'
  	})
  }

  static procent(numerator , denominator) {
  	return numerator / denominator * 100
  }

  static convert_procent(procents) {
    procents = procents / 100;
    var hour = Math.floor((procents*1440) / 60);
    var min = Math.round((procents*1440-hour*60) % 60);
    hour = TimeLine.normalize_time(hour);
    min = TimeLine.normalize_time(min);
    return hour+':'+min
  }

  static pos_to_time(numerator , denominator) {
    return TimeLine.convert_procent(TimeLine.procent(numerator , denominator))
  }

  static normalize_time(str) {
    str = parseInt(str);
    if (str < 10) {
      str = '0'+str.toString();
    }
    if (str == 60) {
      str = 59;
    }
    return str.toString();
  }

  add_current_line() {
  	if (this.current_line != null) {
  	  return;
  	}
  	this.current_line = $('<dir>', {
  	  class: 'current-line'
  	}).appendTo(this.time_line);
  	this.current_line.css(this.STANDART_CSS['current-line']);
  	this.current_line.css({
  	  'height': this.time_line.css('height') * 0.6,
      'margin':'0 1px'
  	});
    this.current_line_data = $('<dir>', {
      class: 'current-line-data',
      text:''
    }).appendTo(this.current_line);
    this.current_line_data.css(this.STANDART_CSS['current-line-data']);
    var cl = this.current_line;
    var tl = this.time_line
    this.time_line.mousemove(function(event) {
      if (event.pageX - $(this).offset().left > $(this).width()) {
        return;
      }
      if (event.pageX - $(this).offset().left < 0) {
        return;
      }
      cl.css({'left': event.pageX-$(this).offset().left-2});
    });
    this.time_line.mouseleave(function() {
      cl.css({'display': 'None'});
    });
    this.time_line.mouseenter(function() {
      cl.css({'display': 'block'});
    });
  }

  static change_current_line_color(elem, hex) {
      if(elem == null){
        return;
      }
      elem.css({
        'background': `linear-gradient(0deg, ${TimeLine.convertHex(hex, 50)} 0%, rgba(0,212,255,0) 80%)`,
      })
  }

  static convertHex(hex,opacity) {
      hex = hex.replace('#','');
      var r = parseInt(hex.substring(0,2), 16);
      var g = parseInt(hex.substring(2,4), 16);
      var b = parseInt(hex.substring(4,6), 16);
      return 'rgba('+r+','+g+','+b+','+opacity/100+')';
  }

}