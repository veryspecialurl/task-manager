// Button Icon
button = '<a href="#" class="ims_switch_button closed" data-status="close">\
            <svg class="bi bi-caret-right" width="28px" height="29px" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">\
              <path fill-rule="evenodd" d="M6 12.796L11.481 8 6 3.204v9.592zm.659.753l5.48-4.796a1 1 0 000-1.506L6.66 2.451C6.011 1.885 5 2.345 5 3.204v9.592a1 1 0 001.659.753z" clip-rule="evenodd"/>\
            </svg>\
          </a>'

$(document).ready(function() {
  // Get Dom elements
  var menu = $('.ims_side_menu');
  menu.addClass('shadow');
  var main_body = $('.main_body');
  main_body.addClass('pr-3');
  // Hide menu
  menu.css({'left':'-'+main_body.css('width')});
  // Get primary body and add open/close open
  var primary_body = $('<div>', {class:'primary_body'}).appendTo(menu);
  var switch_button = primary_body.append(button);
  // On button click
  switch_button.find('a').click(function(){
  	// If closed
    if($(this).hasClass('closed') == true) {
      // Open
      $(this).addClass('opened');
      $(this).removeClass('closed');
    	menu.animate({'left':'0px'});
      $(this).css({'transform' : 'rotate(180deg)'});
    } else { // If opened
      // CLose
    	menu.animate({'left':'-'+main_body.css('width')});
      $(this).addClass('closed');
      $(this).removeClass('opened');
      $(this).css({'transform' : 'rotate(0deg)'});
    }
  });
});