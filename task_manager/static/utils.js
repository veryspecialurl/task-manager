export class Time {
	
	constructor (hour=0, minute=0, second=0, msecond=0) {
		this.hour = hour;
		this.minute = minute;
		this.second = second;
		this.msecond = msecond;
	}

	comparable() {
		return ((this.hour * 60 + this.minute)*60 + this.second) * 1000 + this.msecond;
	}

	in_minutes() {
		return this.hour * 60 + this.minute;
	}

	addHour(hours) {
		this.hour += hours;
		if (this.hour > 24) {
			this.hour = 24;
		}
		if (this.hour < 0) {
			this.hour = 0;
		}
	}

	addMinute(minutes) {
		this.minute += minutes;
		if (this.minute > 60) {
			this.addHour(parseInt(this.minute / 60));
			this.minute = this.minute % 60;
		}
		if (this.minute < 0) {
			this.addHour(parseInt(this.minute / 60));
			var negative_minute = this.minute % 60;
			this.minute = 60 - negative_minute;
		}
	}

	static fromDateInstance(object) {
		return new Time(object.getHours(), object.getMinutes(), object.getSeconds(), object.getMilliseconds());
	}

	// returns 0 if a=b, -1 if a < b, 1 if a > b
	static compare(a, b) {
		if (a.comparable() == b.comparable()) {
			return 0;
		}
		if (a.comparable() > b.comparable()) {
			return 1;
		}
		if (a.comparable() < b.comparable()) {
			return -1;
		}
	}

	static strftime(object, str='%H:%M:%S:%s'){
		// %m - 00-60
		// %M - 0-60
		// %H - 0-24
		// %h - 00-24


		// make double minutes
		var double_minute = ''
		if(object.minute < 10) {
			double_minute = '0'+object.minute.toString();
		} else {
			double_minute = object.minute.toString();
		}

		// make double hours
		var double_hour = ''
		if(object.hour < 10) {
			double_hour = '0'+object.hour.toString();
		} else {
			double_hour = object.hour.toString();
		}

		return str.replace('%H', object.hour.toString()).replace('%h', double_hour).replace('%M', object.minute.toString()).replace('%m', double_minute).replace('%S', object.second.toString()).replace('%s', object.msecond.toString())
	}

	static strptime(object_st, str) {
		var pattern = new RegExp(str.replace('%H', '(\\d*)').replace('%M', '(\\d*)').replace('%S', '(\\d*)').replace('%s', '(\\d*)'));
		var str_pattern = new RegExp(str.replace('%H', '(..)').replace('%M', '(..)').replace('%S', '(..)').replace('%s', '(..)'));
		var mathes_numbers = pattern.exec(object_st);
		var mathes_words = str_pattern.exec(str);
		var hour = 0;
		var minute = 0;
		var second = 0;
		var msecond = 0;
		mathes_numbers.forEach(function(number, i) {
			if (mathes_words[i] == '%H') {
				hour = parseInt(number);
			}
			else if (mathes_words[i] == '%M') {
				minute = parseInt(number);
			}
			else if (mathes_words[i] == '%S') {
				second = parseInt(number);
			}
			else if (mathes_words[i] == '%s') {
				msecond = parseInt(number);
			}
		});
		return new Time(hour, minute, second, msecond)
	}
}

var DAY_OF_WEEKS = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];
var MONTH_NAMES_I = ['Null', 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
var MONTH_NAMES_R = ['Null', 'Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];

export class DateU extends Date {
	//accept Date instance of standart library
	// %Y - 0000-9999* number of year
	// %M - 0-12 number of month
	// %m - 00-12 number of month
	// %mi - human readable month (именительный падеж)
	// %mr - human readable month (родительный падеж падеж)
	// %D - 0-31 day of month
	// %d - 00-31 day of week number
	// %dh - day of week human readable

	constructor(year=null, month=null, day=null, hour=null, minute=null, second=null, millisecond=null) {
	    super(year, month, day, hour, minute, second, millisecond);
	}

	addDay(days){
		this.setDate(this.getDate()+days);
	}

	static strfdate(date, str) {

		var double_month = ''
		if(date.getMonth()+1 < 10) {
			double_month = '0'+(date.getMonth()+1).toString();
		} else {
			double_month = (date.getMonth()+1).toString();
		}

		var double_day = ''
		if(date.getDate() < 10) {
			double_day = '0'+(date.getDate()).toString();
		} else {
			double_day = (date.getDate()).toString();
		}

		return str.replace('%Y', date.getFullYear()).replace('%mr', MONTH_NAMES_R[date.getMonth()]).replace('%mi', MONTH_NAMES_I[date.getMonth()]).replace('%m', double_month).replace('%M', date.getMonth()+1).replace('%D', date.getDate()).replace('%dh', DAY_OF_WEEKS[date.getDay()]).replace('%d', double_day);
	}

	static strpdate(object_st, str) {
		var pattern = new RegExp(str.replace('%Y', '(\\d*)').replace('%m', '(\\d*)').replace('%d', '(\\d*)'));
		var str_pattern = new RegExp(str.replace('%Y', '(..)').replace('%m', '(..)').replace('%d', '(..)'));
		var mathes_numbers = pattern.exec(object_st);
		var mathes_words = str_pattern.exec(str);
		var hour = 0;
		var minute = 0;
		var second = 0;
		mathes_numbers.forEach(function(number, i) {
			if (mathes_words[i] == '%Y') {
				hour = parseInt(number);
			}
			else if (mathes_words[i] == '%m') {
				minute = parseInt(number);
			}
			else if (mathes_words[i] == '%d') {
				second = parseInt(number);
			}
		});
		return new DateU(hour, minute, second)
	}

	static is(date1, date2){
		if (date1.getFullYear() == date2.getFullYear() && date1.getMonth() == date2.getMonth() && date1.getDate() == date2.getDate()) {
			return true;
		} else {
			return false;
		}
	}

	// returns 0 if a=b, -1 if a < b, 1 if a > b
	static compare(date1, date2) {
		if (date1.getFullYear() > date2.getFullYear()) {
			return 1;
		}
		else if (date1.getFullYear() == date2.getFullYear()) {
			if (date1.getMonth() > date2.getMonth()) {
				return 1;
			}
			else if (date1.getMonth() == date2.getMonth()) {
				if(date1.getDate() > date2.getDate()) {
					return 1;
				}
				else if (date1.getDate() == date2.getDate()) {
					return 0;
				}
				else if (date1.getDate() < date2.getDate()) {
					return -1;
				}
			}
			else if (date1.getMonth() < date2.getMonth()) {
				return -1;
			}
		}
		else if (date1.getFullYear() < date2.getFullYear()) {
			return -1
		}
	}

	static copy(date){
		var lasf = new DateU(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds());
		return lasf
	}
}