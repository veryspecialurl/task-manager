export function create_popup_message(text, level='info', timeout=1000, func=function(){}) {
    var swindow = $('<div>', {
        class: 'popup_message rounded shadow d-block mx-auto'
    }).appendTo('.main')
    func();
    swindow.css({
        position: 'absolute',
        'background-color': '#313131',
        display: 'none',
        width: $(window).width() * 0.6,
        height: $(window).width() * 0.2,
        left: ($(window).width() * 0.4) / 2,
        top: $('html').offset().top + ($(window).height() * 0.8) / 2,
        color: 'white',
        'z-index': '5'
    })
    $('<div>', {class:'row m-0', style:"height: 100%;"}).appendTo(swindow);
    $('<div>', {class:'col p-3 d-inline-block', style:"height: 100%;"}).appendTo(swindow.find('.row'));
    $('<span>', {class:`p-3 rounded border border-${level} d-xl-flex justify-content-xl-center align-items-xl-center`, style:"height: 100%;"}).text(text).appendTo(swindow.find('.col'));
    swindow.show('fast');
    setTimeout(function() {
        swindow.hide('fast', function(){
            swindow.remove();
        });
    }, timeout);
}

export function create_choose_window(text, submit, insert = function() {}) {
    var swindow = $('<div>', {
        class: 'popup_choose shadow'
    }).appendTo('.main')
    swindow.css({
        position: 'absolute',
        'background-color': '#313131',
        display: 'none',
        width: $(window).width() * 0.4,
        height: $(window).width() * 0.15,
        left: ($(window).width() * 0.6) / 2,
        top: $('html').offset().top + ($(window).height() * 0.85) / 2,
        color: 'black',
        'z-index': '5',
        padding: '40px'
    })
    swindow.show('fast');
    var row = $('<div>', {'class':'row align-items-center'}).appendTo(swindow);
    row.css('height','100%');
    var col = $('<div>', {'class':'col align-self-center'}).appendTo(row);
    $('<p>', {'class':'text-center text-light'}).text(text).appendTo(col);
    insert();
    var flex_div = $('<div>', {class:'d-flex flex-row justify-content-around'}).appendTo(col);
    $('<button>', {
        type:"button",
        'class': 'submit-window btn btn-danger',
        'text': 'Подтвердить'
    }).appendTo(flex_div);
    $('<button>', {
        type:"button",
        'class': 'cancel-window btn btn-secondary',
        'text': 'Отмена'
    }).appendTo(flex_div);
    $('.submit-window').css({
        height: '50px',
        width:'10em'
    })
    $('.cancel-window').css({
        height: '50px',
        width:'10em'
    })
    $('.submit-window').click(function(event) {
        submit();
        swindow.hide('fast');
        setTimeout(function() {
            swindow.remove();
        }, 500);
    });
    $('.cancel-window').click(function(event) {
        swindow.hide('fast');
        setTimeout(function() {
            swindow.remove();
        }, 500);
    });
}

var STANDART_CSS = {
    'popup_window': {
        'position': 'absolute',
        'background-color': '#313131',
        'min-height': '20%',
        'min-width': '30%',
        'display': 'none'
    },
    "header": {
        'height': '2em',
        'background-color': '#8787ff'
    },
    'header-name': {
        'font-size': '0.8em',
        'color': 'white'
    },
}

export class popup_window {

    constructor(title, height = null, width = null, kwargs) {
        this.kwargs = kwargs
        this.popup_window = $('<div>', {
            'class': 'popup-window-js-class-identificator-button shadow'
        }).css(STANDART_CSS['popup_window']);
        if (height != null) {
            this.popup_window.css({
                'height': height,
            })
        }
        if (width != null) {
            this.popup_window.css({
                'width': width,
            })
        }
        this.header = $('<div>', {
            'class': 'row m-0'
        }).css(STANDART_CSS['header']).appendTo(this.popup_window);
        this.header_inner = $('<div>', {
            'class': 'col d-flex flex-row justify-content-between'
        }).appendTo(this.header);
        this.header_name = $('<span>', {
            'class': 'd-block'
        }).text(title).appendTo(this.header_inner);
        this.header_close = $('<a>', {
            'href': '#'
        }).text('Close').appendTo(this.header_inner);
        this.body_row = $('<div>', {
            'class': 'row'
        }).appendTo(this.popup_window);
        this.body = $('<div>', {
            'class': 'col'
        }).appendTo(this.body_row);
        this.load_events();
    }
    

    show() {
        popup_window.delete_all();
        this.popup_window.appendTo($('body'))
        this.popup_window.fadeIn(100);
        STANDART_CSS['header-name']['line-height'] = `${this.header_inner.height()}px`;
        this.header_name.css(STANDART_CSS['header-name']);
        this.header_close.css(STANDART_CSS['header-name']);
        this.popup_window.css({
            left: $(window).width() / 2 - this.popup_window.width() / 2,
            top: $(window).height() / 2 + $('html').offset().top - this.popup_window.height() / 2,
        }); 
    }

    load_events() {
        this.header_close.click(function(event) {
            popup_window.delete_all();
        });
    }

    delete() {
        this.popup_window.remove();
    }

    static delete_all() {
        $('.popup-window-js-class-identificator-button').fadeOut(100, function(){
            this.remove();
        });
    }

}