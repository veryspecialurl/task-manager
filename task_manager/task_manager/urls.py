from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import RedirectView
import datetime as dt

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('authentication.urls')),
    path('', include('task_module.urls')),
    path('', RedirectView.as_view(url=f'/{dt.datetime.now().year}'), name='main_redirect'),
    path('', include('vksdk.urls'))
]
