from django.shortcuts import render, reverse, redirect
from django.views.generic import View
from .forms import RegisterForm, AuthForm
from django.contrib.auth import login, authenticate, logout
from .models import MyUser
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
import datetime
import logging
import vksdk.auth_manager as VK

file_logger = logging.getLogger('tm.file')


class RegisterView(View):
    def get(self, request):
        context = {
            'register_form': RegisterForm()
        }
        return render(request, 'authentication/register_page.html', context)

    def post(self, request):
        form = RegisterForm(request.POST)
        if form.is_valid():
            MyUser.objects.create_user(email=form.cleaned_data['email'], password=form.cleaned_data['password'])
            user = authenticate(email=form.cleaned_data['email'], password=form.cleaned_data['password'])
            login(request, user)
            return redirect(reverse('tm:year_page', args=[datetime.datetime.now().year]))
        else:
            return render(request, 'authentication/register_page.html', {'register_form': form})


class AuthView(View):
    def get(self, request):
        context = {
            'auth_form': AuthForm()
        }
        return render(request, 'authentication/auth_page.html', context)

    def post(self, request):
        form = AuthForm(request.POST)
        if form.is_valid():
            user = authenticate(email=form.cleaned_data['email'], password=form.cleaned_data['password'])
            if user is not None:
                login(request, user)
                if user.vk is not None:
                    request.session['vk_credentials'] = user.vk
                return redirect(reverse('tm:year_page', args=[datetime.datetime.now().year]))
            else:
                form.add_error('email', 'Такого пользователя не существует')
                return render(request, 'authentication/auth_page.html', {'auth_form': form})
        else:
            return render(request, 'authentication/auth_page.html', {'auth_form': form})


@method_decorator(login_required, name='dispatch')
class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect(reverse('auth:auth_page'))


@method_decorator(login_required, name='dispatch')
class VkRequest(View):
    def get(self, request):
        return redirect(VK.Auth.make_url(redirect_uri=request.build_absolute_uri(reverse("auth:vk_response")),
                                         scope_list='email',
                                         display='popup'))


@method_decorator(login_required, name='dispatch')
class VkResponse(View):
    def get(self, request):
        user_credentials = VK.Auth.auth(request.build_absolute_uri(reverse("auth:vk_response")),
                                        code=request.GET.get('code'))
        request.session['vk_credentials'] = user_credentials.session_serialize()
        request.user.vk = request.session['vk_credentials']
        request.user.save()
        request.session.modified = True
        return redirect(reverse('tm:settings_page'))


@method_decorator(login_required, name='dispatch')
class Vk_logout(View):
    def get(self, request):
        request.user.vk_id = None
        del request.session['vk_credentials']
        request.user.save()
        return redirect(reverse('tm:settings_page'))
