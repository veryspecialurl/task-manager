from django.forms import ModelForm
from .models import MyUser
from django import forms
import logging

logger = logging.getLogger('tm.file')


class RegisterForm(ModelForm):

    password2 = forms.CharField(label='Повторите пароль', widget=forms.PasswordInput(
        attrs={'class': 'InputField', 'placeholder': 'Повторите пароль...'}))

    class Meta:
        model = MyUser
        fields = [
            'email',
            'password'
        ]

        widgets = {
            'email': forms.TextInput(attrs={'class': 'InputField', 'placeholder': 'Введите Email...'}),
            'password': forms.PasswordInput(attrs={'class': 'InputField', 'placeholder': 'Введите пароль...'})
        }

    def clean(self):
        data = super().clean()
        password = data.get('password')
        password2 = data.get('password2')
        if password and password2:
            if password != password2:
                self.add_error('password', 'Пароли должны совпадать')
                raise forms.ValidationError('Пароли должны совпадать', code='Fields aren\'t equal')


class AuthForm(forms.Form):

    email = forms.EmailField(label='Повторите пароль', widget=forms.EmailInput(
        attrs={'class': 'InputField', 'placeholder': 'Введите Email...'}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'InputField', 'placeholder': 'Введите пароль...'}))

