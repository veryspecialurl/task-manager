import pickle

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import PermissionsMixin


class CustomManager(BaseUserManager):
    def create_user(self, email, password):
        if not email:
            raise ValueError('Please input email address')
        user = MyUser(email=email, password=make_password(password))
        user.save()
        return user

    def create_superuser(self, email, password):
        user = self.create_user(email=email, password=password)
        user.is_staff = True
        return user


class PickleField(models.BinaryField):
    def get_prep_value(self, value):
        return pickle.dumps(value)

    def from_db_value(self, value, expression, connection):
        if value is None:
            return value
        return pickle.loads(value)

    def to_python(self, value):
        if value is None:
            return value
        return pickle.loads(value)

    def value_to_string(self, obj):
        value = self.value_from_object(obj)
        return self.get_prep_value(value).rows


class MyUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=256)
    vk = PickleField(null=True)
    is_staff = models.BooleanField(default=False)

    REQUIRED_FIELDS = ['password']

    USERNAME_FIELD = 'email'

    objects = CustomManager()

    def __str__(self):
        return self.email

    @property
    def is_authenticated(self):
        return True

    @property
    def is_superuser(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def is_admin(self):
        return self.is_staff

    is_admin.boolean = True
