from django.contrib import admin
from .models import MyUser

@admin.register(MyUser)
class AuthAdmin(admin.ModelAdmin):
    list_display = ('id', 'email', 'is_admin')
