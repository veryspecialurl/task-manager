from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.hashers import check_password
from .models import MyUser

class AuthBackend(BaseBackend):
    def authenticate(self, request, email=None, password=None):
        try:
            user = MyUser.objects.get(email=email)
            if check_password(password, user.password):
                return user
        except MyUser.DoesNotExist:
            return None

    def get_user(self, _id):
        try:
            return MyUser.objects.get(pk=_id)
        except MyUser.DoesNotExist:
            return None
