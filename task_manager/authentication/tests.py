from django.test import TestCase, Client
from django.urls import reverse_lazy
from .models import MyUser


class Auth(TestCase):

    def setUp(self):
        self.client = Client()

    def test_register_page(self):
        # testing get method
        response = self.client.get('/register/')
        self.assertEqual(response.status_code, 200)
        # testing post method
        self.client.post(reverse_lazy('auth:register_page'), {
            'email': 'test@email.com',
            'password': '123456',
            'password2': '123456'
        })
        self.assertTrue(MyUser.objects.get(email='test@email.com'))

    def test_auth_page(self):
        response = self.client.get(reverse_lazy('auth:auth_page'))
        self.assertEqual(response.status_code, 200)
        # testing post method
        MyUser.objects.create_user(email='test@email.com', password='123456')
        response = self.client.post(reverse_lazy('auth:auth_page'), {
            'email': 'test@email.com',
            'password': '123456'
        })
        self.assertTrue(response.context is None)