from django.urls import path
from .views import *

app_name = 'auth'
urlpatterns = [
    path('register/', RegisterView.as_view(), name='register_page'),
    path('auth/', AuthView.as_view(), name='auth_page'),
    path('logout/', LogoutView.as_view(), name='logout_page'),
    path('vkloginrequest/', VkRequest.as_view(), name='vk_request'),
    path('vkloginresponse/', VkResponse.as_view(), name='vk_response'),
    path('vklogout/', Vk_logout.as_view(), name='vk_logout')
]